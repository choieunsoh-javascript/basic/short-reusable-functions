// * Create an array of numbers from 1 to numbers
const range = (n) => Array.from({ length: n }, (_, i) => i + 1);
console.log(range(100));

// * Shuffling an array
const shuffle = (arr) => arr.sort(() => 0.5 - Math.random());
console.log(shuffle(range(100)));

// * Remove Duplicated from Array
const removeDuplicated = (arr) => [...new Set(arr)];
console.log(removeDuplicated([...range(10), ...range(10)]));

// * The number is even or not
const isEven = (num) => num % 2 === 0;
console.log(isEven(4));

// * Find the sum of an array
const sum = (arr) => arr.reduce((a, b) => a + b, 0);
console.log(sum(range(20)));

// * Find largest numbers
const findLargest = (arr) => arr.map((subArr) => Math.max(...subArr));
console.log(
  findLargest([
    [4, 5, 1, 3],
    [13, 27, 18, 26],
    [32, 35, 37, 39],
    [1000, 1001, 857, 1],
  ])
);

// * Find the range of an array
const range1 = (arr) => Math.max(...arr) - Math.min(...arr);
console.log(range1([1, 2, 3, 4]));

// * Reverse String
const reverseString = (str) => str.split("").reverse().join("");
console.log(reverseString("hello"));

// * Generate a random color
const getRandomColor = () =>
  `#${Math.floor(Math.random() * 16777215).toString(16)}`;
console.log(getRandomColor());

// * Get selected text
const getSelectedText = () => window.getSelection().toString();
console.log(getSelectedText());

// * Time the execution of your code
console.time("time");
for (let i = 0; i < 1000000; i++) {
  let x = i;
}
console.timeEnd("time");

// * Map an array without using .map()
const map = (arr, cb) => Array.from(arr, cb);
console.log(map([1, 2, 3, 4], (n) => n * 2));

// * Find the intersection of two arrays
const intersection = (arr1, arr2) => {
  const set = new Set(arr1);
  return arr2.filter((x) => set.has(x));
};
console.log(intersection([1, 2, 3], [2, 3, 4]));

// * Remove falsy values from an array
const compact = (arr) => arr.filter(Boolean);
console.log(compact([0, 1, false, 2, "", 3, "a", "e" * 23, NaN, "s", 34]));
